﻿namespace Arbeitszeitrechner
{
    partial class frm_Hauptfenster
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Hauptfenster));
            this.label1 = new System.Windows.Forms.Label();
            this.tmr_currenttime = new System.Windows.Forms.Timer(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txt_derzeitzeit = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txt_Feierabend = new System.Windows.Forms.Label();
            this.cmd_berechnen = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.txt_WorkHours = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txt_Break = new System.Windows.Forms.MaskedTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_StartTime = new System.Windows.Forms.MaskedTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(131, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(413, 51);
            this.label1.TabIndex = 0;
            this.label1.Text = "Work time calculator";
            // 
            // tmr_currenttime
            // 
            this.tmr_currenttime.Tick += new System.EventHandler(this.tmr_currenttime_Tick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txt_derzeitzeit);
            this.groupBox1.Location = new System.Drawing.Point(148, 92);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(358, 96);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Current time";
            // 
            // txt_derzeitzeit
            // 
            this.txt_derzeitzeit.BackColor = System.Drawing.Color.Transparent;
            this.txt_derzeitzeit.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_derzeitzeit.ForeColor = System.Drawing.Color.Black;
            this.txt_derzeitzeit.Location = new System.Drawing.Point(102, 30);
            this.txt_derzeitzeit.Name = "txt_derzeitzeit";
            this.txt_derzeitzeit.Size = new System.Drawing.Size(256, 44);
            this.txt_derzeitzeit.TabIndex = 6;
            this.txt_derzeitzeit.Text = "23:59:59";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txt_Feierabend);
            this.groupBox3.Controls.Add(this.cmd_berechnen);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.txt_WorkHours);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.txt_Break);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.txt_StartTime);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Location = new System.Drawing.Point(35, 205);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(620, 330);
            this.groupBox3.TabIndex = 18;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Calculator";
            // 
            // txt_Feierabend
            // 
            this.txt_Feierabend.BackColor = System.Drawing.Color.Transparent;
            this.txt_Feierabend.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Feierabend.ForeColor = System.Drawing.Color.Black;
            this.txt_Feierabend.Location = new System.Drawing.Point(195, 33);
            this.txt_Feierabend.Name = "txt_Feierabend";
            this.txt_Feierabend.Size = new System.Drawing.Size(373, 53);
            this.txt_Feierabend.TabIndex = 17;
            this.txt_Feierabend.Text = "Calculation is pending";
            // 
            // cmd_berechnen
            // 
            this.cmd_berechnen.Location = new System.Drawing.Point(247, 208);
            this.cmd_berechnen.Name = "cmd_berechnen";
            this.cmd_berechnen.Size = new System.Drawing.Size(147, 75);
            this.cmd_berechnen.TabIndex = 17;
            this.cmd_berechnen.Text = "Calculate";
            this.cmd_berechnen.UseVisualStyleBackColor = true;
            this.cmd_berechnen.Click += new System.EventHandler(this.cmd_Feierabend_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(16, 33);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(171, 32);
            this.label9.TabIndex = 18;
            this.label9.Text = "End of work:";
            // 
            // txt_WorkHours
            // 
            this.txt_WorkHours.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_WorkHours.Location = new System.Drawing.Point(409, 89);
            this.txt_WorkHours.Mask = "90:00";
            this.txt_WorkHours.Name = "txt_WorkHours";
            this.txt_WorkHours.Size = new System.Drawing.Size(159, 75);
            this.txt_WorkHours.TabIndex = 21;
            this.txt_WorkHours.ValidatingType = typeof(System.DateTime);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(424, 164);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(135, 29);
            this.label7.TabIndex = 22;
            this.label7.Text = "Workhours ";
            // 
            // txt_Break
            // 
            this.txt_Break.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Break.Location = new System.Drawing.Point(238, 89);
            this.txt_Break.Mask = "90:00";
            this.txt_Break.Name = "txt_Break";
            this.txt_Break.Size = new System.Drawing.Size(159, 75);
            this.txt_Break.TabIndex = 19;
            this.txt_Break.ValidatingType = typeof(System.DateTime);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(277, 164);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 29);
            this.label6.TabIndex = 20;
            this.label6.Text = "Break";
            // 
            // txt_StartTime
            // 
            this.txt_StartTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_StartTime.Location = new System.Drawing.Point(56, 89);
            this.txt_StartTime.Mask = "90:00";
            this.txt_StartTime.Name = "txt_StartTime";
            this.txt_StartTime.Size = new System.Drawing.Size(159, 75);
            this.txt_StartTime.TabIndex = 17;
            this.txt_StartTime.ValidatingType = typeof(System.DateTime);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(108, 165);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 29);
            this.label5.TabIndex = 18;
            this.label5.Text = "from";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.label2.Location = new System.Drawing.Point(380, 541);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(265, 17);
            this.label2.TabIndex = 19;
            this.label2.Text = "© 2020 Alessio Buda | All rights reserved";
            // 
            // frm_Hauptfenster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(687, 565);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frm_Hauptfenster";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Work time calculator by Alessio Buda";
            this.groupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer tmr_currenttime;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label txt_derzeitzeit;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label txt_Feierabend;
        private System.Windows.Forms.Button cmd_berechnen;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.MaskedTextBox txt_WorkHours;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MaskedTextBox txt_Break;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.MaskedTextBox txt_StartTime;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
    }
}

