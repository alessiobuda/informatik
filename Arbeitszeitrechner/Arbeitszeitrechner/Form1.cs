﻿// Name of Project: Working time calculator
// Creator:         Alessio Buda
// Date:            25.02.2020  
// Description of the program:
// "You can calculate your end of work by simply type in the start of your workday, your breaktime and your usual time of work."
// Attention: Some parts of this program may be in german because of the origin of the program.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Arbeitszeitrechner
{
    public partial class frm_Hauptfenster : Form
    {
        public frm_Hauptfenster()
        {
            InitializeComponent();


            // Enabling Timer
            tmr_currenttime.Enabled = true;
        }

        private void tmr_currenttime_Tick(object sender, EventArgs e)
        {
            // Displaying current time
            DateTime localDate = DateTime.Now;
            txt_derzeitzeit.Text = localDate.ToString("HH:mm");
        }

        private void cmd_Feierabend_Click(object sender, EventArgs e)
        {
            double time = 0;
            time += calculateTime(txt_StartTime.Text);
            time += calculateTime(txt_WorkHours.Text);
            time += calculateTime(txt_Break.Text);
            txt_Feierabend.Text = doubleToTime(time);
        }
        private double calculateTime(string text)
        {
            string[] ints = text.Split(':');
            double hour = Convert.ToDouble(ints[0]) + Convert.ToDouble(ints[1]) / 60;

            return hour;
        }
        private string doubleToTime(double time)
        {
            Console.WriteLine(time.ToString());
            return TimeSpan.FromHours(time).ToString("h\\:mm");
        }
    }
}
